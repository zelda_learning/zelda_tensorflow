from scipy.spatial import distance
import DataParserFinal as dp
import Cancer_Data_Parser as cdp
import numpy as np

def euclid_distance(a, b):
    return distance.euclidean(a, b)


def get_neighbours(k, individual, data):
    k_nearest_neighbours = []
    corr_distances = []
    for i in range(k):
        corr_distances.append(float('inf'))
        k_nearest_neighbours.append([])

    for data_point in data:
        distance = euclid_distance(individual, data_point[1])
        if distance < max(corr_distances):
            index = np.argmax(corr_distances)
            corr_distances[index] = distance
            k_nearest_neighbours[index] = data_point

    return k_nearest_neighbours


def get_percentages(individual, neighbours, categories):
    percentages = []
    total_sum = 0
    for neighbour in neighbours:
        total_sum += euclid_distance(individual, neighbour[1])

    for category in categories:
        group = dp.get_group(category, neighbours)
        group_sum = 0
        for neighbour in group:
            group_sum += euclid_distance(individual, neighbour[1])
        percentage = []
        percentage.append(category)
        percentage.append(group_sum/total_sum)
        percentages.append(percentage)

    return percentages


def evaluate_model(training, testing, k, categories):
    correct = 0
    for indiv in testing:
        k_nearest_neighbours = get_neighbours(k, indiv[1], training)
        percentages = get_percentages(indiv[1], k_nearest_neighbours,categories)
        category = filter_max(percentages)
        if category == indiv[0]:
            correct += 1
    return correct/len(testing)


def filter_max(percentages):
    percentage = percentages[0]
    for p in percentages:
        if p[1] > percentage[1]:
            percentage = p
    return percentage[0]


if __name__ == "__main__":
    # NOTE: For this model to work there needs to be an equal number of data points for each category.
    # In this case there are different number of data points for each category which affects the probabilities
    data = cdp.get_data("C:/Users/Timothy/Desktop/Zelda/data.csv")
    categories = ['M', 'B']
    training, testing = cdp.training_and_testing(data, categories)
    print(evaluate_model(training, testing, 16, categories))







