import csv
import random as r


def get_data(file_path):
    categories = ['radius_mean','texture_mean','perimeter_mean','area_mean','smoothness_mean','compactness_mean','concavity_mean','concave points_mean','symmetry_mean','fractal_dimension_mean']
    all_data = []
    with open(file_path) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        for r in csv_reader:
            group = r['diagnosis']
            if len(group) != 1:
                continue
            row = []
            row.append(group)
            values = []
            for category in categories:
                values.append(r[category])
            values = list(map(float, values))
            row.append(values)
            all_data.append(row)
        return all_data


def get_group(category, arr):
    return list(filter(lambda x: x[0] == category, arr))


def training_and_testing(data, categories):
    # This function is only needed if there is a different number of each group.
    training = []
    testing = []
    training_number = float('inf')
    testing_number = float('inf')
    for category in categories:
        group = get_group(category,data)
        number = int(0.75*(len(group)))
        if number < training_number:
            training_number = number
            testing_number = len(group) - number

    for category in categories:
        group = get_group(category, data)
        training = training + group[:training_number]
        testing = testing + group[training_number: training_number+testing_number]
    r.shuffle(training)
    r.shuffle(testing)
    return training, testing


if __name__ == "__main__":
    data = get_data("C:/Users/Timothy/Desktop/Zelda/data.csv")
    categories = ['M','B']
    training, testing = training_and_testing(data, categories)
    print(training)
    print(testing)
