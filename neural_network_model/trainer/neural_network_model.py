import tensorflow as tf
from tensorflow import keras
from keras.layers import Dropout, Dense, Activation, Convolution1D


def initialise_model(dim, n_outputs ,shape):
    model = keras.Sequential()
    model.add(keras.layers.Dense(64, activation='relu', input_dim=10))
    # model.add(keras.layers.Flatten(input_shape=(10,)))
    model.add(keras.layers.Dense(50, activation=tf.nn.relu))
    # model.add(keras.layers.Dense(6, activation=tf.nn.relu))
    model.add(keras.layers.Dense(n_outputs, activation=tf.nn.softmax))
    model.compile(optimizer=tf.keras.optimizers.Adam(),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    return model

