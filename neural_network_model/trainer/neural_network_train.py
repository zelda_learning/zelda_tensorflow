import numpy as np
import data_parser as dp
from sklearn import preprocessing
import neural_network_model
from tensorflow.python.lib.io import file_io
import pickle
import keras.models as keras_models
from tensorflow import keras
import h5py
import argparse
from tensorflow.python.saved_model import builder as saved_model_builder, tag_constants, signature_constants
from tensorflow.python.saved_model.signature_def_utils_impl import predict_signature_def

def get_predictor_and_response(data):
    response = []
    predictor = []
    for d in data:
        response.append(d[0])
        predictor.append(d[1])
    return predictor, response


def standardise(vector):
    min_max_scalar = preprocessing.MinMaxScaler()
    transformed_vector = min_max_scalar.fit_transform(vector)
    return transformed_vector


def train_model(model, train_predictors, train_labels, epochs):
    model.fit(train_predictors, train_labels, epochs=epochs)
    return model


def test_accuracy(model, testing):
    test_predictor, test_response = get_predictor_and_response(testing)
    standardised_test_predictors = standardise(test_predictor)
    converted_test_labels = convert_labels(test_response)
    test_loss, test_acc = model.evaluate(standardised_test_predictors, converted_test_labels)
    return test_acc


def convert_labels(labels):
    numeric_labels = []
    for label in labels:
        if label == 'M':
            numeric_labels.append(0)
        else:
            numeric_labels.append(1)
    return numeric_labels


def save_model(trained_model, config):
    # Model Saving (Tensorflow .pb file for cloud prediction)
    keras.backend.set_learning_phase(0)
    model_config = trained_model.get_config()
    model_weights = trained_model.get_weights()
    new_model = keras.Sequential.from_config(model_config)
    new_model.set_weights(model_weights)
    export_path = config['trainingInput']['jobDir'] + "/export"
    builder = saved_model_builder.SavedModelBuilder(export_path)
    signature = predict_signature_def(
        inputs={'input': new_model.inputs[0]},
        outputs={'output': new_model.outputs[0]})

    with keras.backend.get_session() as sess:
        builder.add_meta_graph_and_variables(
            sess=sess,
            tags=[tag_constants.SERVING],
            clear_devices=True,
            signature_def_map={signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature}
        )
    builder.save()

    print(new_model.outputs)
    print(export_path)


    # print(model_weights)
    # print(model_config)


if __name__ == "__main__":
    argumentParser = argparse.ArgumentParser()
    argumentParser.add_argument(
        '--train-file',
        help='GCS or local paths to training data',
        required=False  # For now
    )
    argumentParser.add_argument(
        '--job-dir',
        help='GCS location to write checkpoints and export models',
        required=False  # For now
    )
    args = argumentParser.parse_args()
    arguments = args.__dict__
    print(arguments)

    config = dp.load_config('config.yml')
    data = dp.get_data(config)
    responses = config['data']['availableResponses']
    training, testing = dp.training_and_testing(data, responses)
    train_predictor, train_labels = get_predictor_and_response(training)
    standardised_predictors = standardise(train_predictor)
    num, dim = np.shape(standardised_predictors)
    untrained_model = neural_network_model.initialise_model(dim, 2, np.shape(standardised_predictors))
    print(standardised_predictors)

    converted_trained_labels = convert_labels(train_labels)
    trained_model = train_model(untrained_model, standardised_predictors, converted_trained_labels, 50)
    save_model(trained_model, config)
    print('Test Accuracy:', test_accuracy(trained_model, testing))
