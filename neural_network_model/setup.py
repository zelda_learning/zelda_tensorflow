from setuptools import find_packages
from setuptools import setup

REQUIRED_PACKAGES = ['tensorflow', 'keras', 'h5py']

setup(
    name='trainer',
    version='0.1',
    install_requires=REQUIRED_PACKAGES,
    packages=find_packages(),
    include_package_data=True,
    description='Neural network for classifying an individual into a certain category based on probability',
    author='Timothy Simons',
    author_email='timothysimons75@gmail.com'
)
