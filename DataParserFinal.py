import csv


def get_data(file_path):
    questions = ['E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9', 'E10', 'N1', 'N2', 'N3', 'N4', 'N5', 'N6', 'N7',
                 'N8', 'N9', 'N10', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'C1', 'C2', 'C3', 'C4',
                 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'O1', 'O2', 'O3', 'O4', 'O5', 'O6', 'O7', 'O8', 'O9', 'O10']
    all_data = []
    with open(file_path) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=";")
        for r in csv_reader:
            group = r["age"]
            if len(group) != 2:
                continue
            row = []
            row.append(int(group[0]))
            values = []
            for category in questions:
                values.append(r[category])
            values = list(map(int, values))
            row.append(convert_to_scores(values))
            all_data.append(row)
        return all_data


def convert_to_scores(list):
    i, j = 0, 10
    score_list = []
    for num in range(5):
        score_list.append(sum(list[i:j]))
        i = j
        j += 10
    return score_list


def get_group(number, arr):
    return list(filter(lambda x: x[0] == number, arr))


def get_multivariate_dist(group_array, dim):
    # group array needs to be of the form [[1,[...]],[1,[...]],...]
    multi_distribution = []
    for i in range(dim):
        multi_distribution.append([])
    for individual in group_array:
        scores = individual[1]
        for i in range(len(scores)):
            multi_distribution[i].append(scores[i])
    return multi_distribution


if __name__ == "__main__":
    all_data = get_data('C:/Users/Timothy/Desktop/Zelda/big_data_set.csv')
    print(all_data)
    group = get_group(1, all_data)