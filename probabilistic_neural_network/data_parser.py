import csv
import random as r
import yaml


def load_config(config_file):
    with open(config_file, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


def get_data(config):
    file_path = config['data']['datasetPath']
    predictor_categories = config['data']['predictorCategories']
    all_data = []
    with open(file_path) as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=",")
        for r in csv_reader:
            group = r[config['data']['responseCategory']]
            if len(group) != 1:
                continue
            row = []
            row.append(group)
            values = []
            for category in predictor_categories:
                values.append(r[category])
            values = list(map(float, values))
            row.append(values)
            all_data.append(row)
        return all_data


def get_group(category, arr):
    return list(filter(lambda x: x[0] == category, arr))


def training_and_testing(data, responses):
    # This function is only needed if there is a different number of each group.
    training = []
    testing = []
    training_number = float('inf')
    testing_number = float('inf')
    for category in responses:
        group = get_group(category,data)
        number = int(0.75*(len(group)))
        if number < training_number:
            training_number = number
            testing_number = len(group) - number

    for category in responses:
        group = get_group(category, data)
        training = training + group[:training_number]
        testing = testing + group[training_number: training_number+testing_number]
    r.shuffle(training)
    r.shuffle(testing)
    return training, testing


if __name__ == "__main__":
    config = load_config('config.yml')
    data = get_data(config)
    available_responses = config['data']['availableResponses']
    training, testing = training_and_testing(data, available_responses)
    print(training)
    print(testing)
