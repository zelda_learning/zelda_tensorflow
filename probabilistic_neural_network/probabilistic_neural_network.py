from scipy.stats import kde
import data_parser as dp
import argparse
import pickle
import json


def write_json(individual):
    with open('individual.json', 'w') as outfile:
        json.dump(individual, outfile)


def read_json(json_file_path):
    with open(json_file_path) as json_file:
        individual = json.load(json_file)
        return individual

def save_object(object, config_file_name):
    with open(config_file_name, 'wb') as file_name:
        pickle.dump(object, file_name)


def load_object(config_file_name):
    with open(config_file_name, 'rb') as file_name:
        pdfs = pickle.load(file_name)
        return pdfs


def get_group(number, arr):
    return list(filter(lambda x: x[0] == number, arr))


def get_multivariate_dist(group_array, dim):
    # group array needs to be of the form [[1,[...]],[1,[...]],...]
    multi_distribution = []
    for i in range(dim):
        multi_distribution.append([])
    for individual in group_array:
        scores = individual[1]
        for i in range(len(scores)):
            multi_distribution[i].append(scores[i])
    return multi_distribution


def obtain_pdf(multivariate_data):
    return kde.gaussian_kde(multivariate_data)


def get_pdfs(categories, dim, data):
    group_multi_data = get_group_multi_data(categories, dim, data)
    return list(map(obtain_pdf, group_multi_data))


def get_group_multi_data(categories, dim, all_data):
    group_multi_data = []
    for i in categories:
        group = dp.get_group(i, all_data)
        multi = get_multivariate_dist(group, dim)
        group_multi_data.append(multi)
    return group_multi_data


def evaluate_individual(individual, all_group_pdfs):
    probabilities = []
    for dist in all_group_pdfs:
        prob = dist.pdf(individual)[0]
        probabilities.append(prob)
    return probabilities


def get_percentages(probabilities):
    sum_of_prob = sum(probabilities)
    return list(map(lambda x: x/sum_of_prob, probabilities))


def evaluate_model(testing, categories, pdfs):
    correct = 0
    for indiv in testing:
        probabilities = evaluate_individual(indiv[1], pdfs)
        percentages = get_percentages(probabilities)
        category = filter_max(percentages, categories)
        if category == indiv[0]:
            correct += 1
    return correct/len(testing)


def filter_max(percentages, categories):
    index = float('inf')
    percentage = 0
    for i in range(len(percentages)):
        if percentages[i] > percentage:
            percentage = percentages[i]
            index = i
    return categories[index]


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        '--train',
        help='Boolean indicating the program to train and save the model',
        required=False,
        default='False'
    )
    argument_parser.add_argument(
        '--predict-location',
        help='Specifies the location of the JSON file needed for prediction',
        required=False,
        default='./individual/individual.json'
    )

    config = dp.load_config('config.yml')
    data = dp.get_data(config)
    categories = config['data']['availableResponses']
    training, testing = dp.training_and_testing(data, categories)
    dim = len(config['data']['predictorCategories'])
    pdfs = get_pdfs(categories, dim, training)

    args = argument_parser.parse_args()
    if (args.train is True and args.predict_location is not None) or (args.train is False and args.predict_location is None):
        argument_parser.error("one (and only one) of --train and --predict_location is required")
    elif args.train == "True":
        save_object(pdfs, config['model']['savedpdfFile'])
    elif args.predict_location is not None:
        individual = read_json(args.predict_location)
        print(get_percentages(evaluate_individual(individual,pdfs)))
    else:
        print(evaluate_model(testing, categories, pdfs))



